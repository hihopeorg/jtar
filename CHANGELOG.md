## v1.0.2

- 适配DevEco Studio 3.1Beta1及以上版本。
- 适配OpenHarmony SDK API version 9及以上版本。
- 重构工程读写逻辑，不再支持直接命令行操作，采用文件读写操作

## v1.0.1

- api8升级到api9，并转换为stage模型

## v1.0.0

1. 支持tar打包

2. 支持tar解包

