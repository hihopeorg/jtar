# Jtar

## 简介
> Jtar支持tar打包和tar解包功能。

## 效果展示
![avatar](screenshot/app.jpeg)![avatar](screenshot/cmd.jpg)

## 下载安装
```shell
npm install @ohos/jtar --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

1. 在page页面引入包

 ```
import {JTar} from "../jtar.ets"
 ```
2. 创建tartest对象

 ```
   let fileName: string = "tartest";
   let untarDirName: string = "tartestdir";
   let tartest: JTar = new JTar(fileName, untarDirName);
 ```
3. 初始化Tar测试环境，创建生成tartestdir目录，并且包含一些txt文件

 ```
   JtarTestInit.TestTarInit();
 ```
4. Tar功能测试，运行后会将环境中的tartestdir目录打包为tartest.tar文件

 ```
   tartest.tar();
 ```
5. 初始化Untar测试环境

 ```
   JtarTestInit.TestUnTarInit();
 ```
6. Untar功能测试，运行后会将环境中的tartest.tar文件解包

 ```
   tartest.untar();
 ```

## 以上功能测试均可通过在设备应用目录中查看

在设备shell中:

1. 进入到应用files目录中

```
   # cd /data/app/el2/100/base/cn.openharmony.jtar/haps/entry/files
```
2. 点击Tar Init，会生成tartestdir，tartestdir中包含一个txt文件

```
   # ls
   tartestdir
   # ls tartestdir/
   test.txt
```
3. 点击Tar，会打包生成对应的tartest.tar文件

```
   # ls
   tartest.tar  tartestdir
```
4. 点击UnTar Init，生成对应的tartest.tar文件

```
   # ls
   tartest.tar
```
5. 点击UnTar，生成对应的tartest.tar文件的原始文件

```
   # ls
   tartest.tar  tartestdir
   # ls tartestdir
   test.txt
```

## 接口说明

1. 修改生成的tar文件名
   `setTarName(name: string)`
   
2. 修改解包目标的Untar文件名
   `setUnTarName(name: string)`
   
3. 修改解包目标的Untar路径
   `setUnTarPath(path: string)`

4. 添加一个需要tar的文件或目录
   `addTarPath(path: string)`

5. 删除一个需要tar的文件或目录
   `delUnTarPath(path: string)`

6. tar打包
   `tar(): number`

8. untar解包
   `untar(path?: string): number`


## 兼容性
- [DevEco Studio版本](https://developer.harmonyos.com/cn/develop/deveco-studio#download)：DevEco Studio 3.1Beta1及以上版本。
- OpenHarmony SDK版本：API version 9及以上版本。

## 目录结构
````
|---- jtar  
|     |---- entry  # 示例代码文件夹
|     |---- jtar  # jtar库文件夹
|        |---- src 
|           |---- main 
|                 |---- cpp  # c++详细接口
|                 |---- ets
|                       |---- jtar.ets  # 接口封装
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/jtar/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/jtar/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/jtar/blob/master/LICENSE) ，请自由地享受和参与开源。